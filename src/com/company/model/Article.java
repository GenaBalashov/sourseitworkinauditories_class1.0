package com.company.model;

public class Article {
    public String title;
    public String text;
    public String url;
    public Comment[] comments;
}
