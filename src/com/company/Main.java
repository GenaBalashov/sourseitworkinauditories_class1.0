package com.company;

import com.company.model.Article;
import com.company.model.Comment;


public class Main {

    public static void main(String[] args) {
        Article[] articles = new Article[5];

        //generate
        for (int i = 0; i < 5; i++) {
            Article article = new Article();
            article.title = "title " + i;
            article.text = "text " + i;
            article.url = "http://images.com/image  " + i + ".jpg";
            article.comments = new Comment[3];

            for (int j = 0; j < 3; j++) {
                Comment comment = new Comment();
                comment.autor = "Vasya " + j;
                comment.text = "comment " + j;
                comment.rating = j;
                article.comments[j] = comment;
            }

            articles[i] = article;
        }

        //print
        for (Article article : articles) {
            System.out.println("----");
            System.out.println(article.text);
            System.out.println(article.title);
            System.out.println(article.url);
            for (Comment comment : article.comments) {
                System.out.println("---comments---");
                System.out.println(comment.autor);
                System.out.println(comment.text);
                System.out.println(comment.rating);
            }
        }
    }
}
